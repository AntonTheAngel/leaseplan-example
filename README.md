# leaseplan-example Framework

This is a sample Rest API test solution for sample endpoints available in  https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} The published APIs has used to search  product from the list of "apple", "mango", "tofu", "water". Other searches will return error.

Tests are written using a combination of SerenityBDD, RestAssured, Cucumber, Junit & Maven.

## Technology Stack

- Java
- Serenity BDD
- Maven
- RestAssured

## Prerequisites

* [Java 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java Dev Kit
* [Maven](https://maven.apache.org/download.cgi) - Dependency Manager

## Application Under Test

We are using the below API as the Application Under Test.

* URL : https://waarkoop-server.herokuapp.com/api/v1/search/test/{product}

## The project directory structure
The project follows the standard directory structure used in most Serenity projects:

```Gherkin
src
  + main
    + java                          
      + env                         methods to get/set env related configs
      + utilities                   utility methods and constants
  + test
    + java                          
      + endpoints                   endpoints of the services
      + runners                     test runner(senerity runner/trigger configurations)
      + steps             			 Step definitions for the BDD feature
      + utils                       Common utility methods
    + resources
      + features                  	 Feature files
      + properties               	 AUT properties files
      + logback.xml               	 for debug logs
```
Following instructions will help you running the project. First, clone this project locally on your machine from the master branch.

### Installation and Test Execution

Open the project in any IDE Eclipse/IntelliJ. Run the following command in Terminal and build the project. It will automatically download all the required dependencies.
 
mvn clean install

### Execute Tests

Run the following command in Terminal to execute tests.

 
mvn clean verify


### Write new Test

If you want to create new test cases please add them in "search-product.feature"
 

### Test Report

You can find the Serenity reports in the following directory of the Project
target/site/serenity/index.html

Download artifacts from https://gitlab.com/AntonTheAngel/leaseplan-example/-/pipelines and extract to see the html report


### What was refactored and why


1. Removed gradle related files, I wanted to keep only Maven, so I removed them.
2. .feature file under /search folder in /features folder. But wrongly mentioned in test runner class updated that. 
3. Added glue for the step definitions. 
4. Changed package names.
5. Updated README.md
6. Removed README.md under the package src/main/java/starter
7. I have added org.json in pom.xml
8. Updated project name in serenity.properties
9. feature file name is updated.
10. Hidden files are removed


### Repository and CI CD

https://gitlab.com/AntonTheAngel/leaseplan-example/

https://gitlab.com/AntonTheAngel/leaseplan-example/-/pipelines
