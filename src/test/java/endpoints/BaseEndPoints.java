package endpoints;

 
import java.util.List; 
import org.json.JSONObject;
import org.junit.Assert; 
import env.ApplicationProperties;
import env.Environment;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import utils.CommonUtils;
import utils.Constants;

public class BaseEndPoints {
	ApplicationProperties appProps = Environment.INSTANCE.getApplicationProperties();

	/**
	 * common specification for request
	 */
	public RequestSpecification getCommonSpec() {
		RequestSpecification rSpec = SerenityRest.given();
		rSpec.contentType(ContentType.JSON).baseUri(appProps.getBaseURL());
		return rSpec;
	}

	/**
	 * Verify that the response code is the same as expected code by comparing the
	 * provided expected code and the response code from the response received by
	 * sending the request
	 */
	public void verifyResponseStatusCode(Response response, int expectedCode) {
		Assert.assertEquals(response.getStatusCode(), expectedCode);
	}

	/**
	 * Send request
	 * 
	 * @param request     details for sending the request
	 * @param requestType of the request. i.e GET, POST, PUT, DELETE, UPDATE
	 * @param url         to execute for the request
	 * @param pojo        if provided will be added to the body of request as JSON
	 *                    payload
	 * @return response received from the service by sending the request
	 */
	@Step
	public Response sendRequest(RequestSpecification request, int requestType, Object pojo) {
		Response response;


		// need to add a switch based on request type
		switch (requestType) {
		case Constants.RequestType.POST_REQUEST:
			if (request == null) {
				response = SerenityRest.when().post();
			} else {
				response = request.post();
			}
			break;
		case Constants.RequestType.DELETE_REQUEST:
			if (request == null) {
				response = SerenityRest.when().delete();
			} else {
				response = request.delete();
			}
			break;
		case Constants.RequestType.PUT_REQUEST:
			if (request == null) {
				response = SerenityRest.when().put();
			} else {
				response = request.put();
			}
			break;
		case Constants.RequestType.GET_REQUEST:
		default:
			if (request == null) {
				response = SerenityRest.when().get();
			} else {
				response = request.get();
			}
			break;
		}
		return response;
	}

	/**
	 * Verify that the response data contains the expected product from the response
	 * received by sending the request
	 * Searching all the products returned.
	 */
	public void verifyResponseData(Response response, String product) {

		List<String> jsonResponse = null;

		boolean isDataPresent = false;

		if (product.equals("apple") || product.equals("mango") || product.equals("tofu")
				|| product.equals("water")) {
			jsonResponse = response.jsonPath().getList("title");

			System.out.println("size --- " + jsonResponse.size());

			isDataPresent = CommonUtils.containsIgnoreCase(jsonResponse, product);
		}

		Assert.assertTrue(isDataPresent);
	}
 

	/**
	 * Verify that the response data for the wrong product from the response
	 * received by sending the request
	 */
	public void verifyResponseDataNotPresent(Response response, String product) {
		
		boolean isDataNotPresent = false;

		if (!product.equals("apple") || !product.equals("mango")
				|| !product.equals("tofu") || !product.equals("water")) {

			String errorMessage = response.jsonPath().getString("detail.error");

			System.out.println("errorMEssage " + errorMessage);

			isDataNotPresent = errorMessage.equals("true");
		}
		
		Assert.assertTrue(isDataNotPresent);
	}

}
