package endpoints;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utils.Constants;

public class SearchProductEndPoints extends BaseEndPoints {
	
	/**
	 * This class is the implementation of search product specific end points 
	 */
	
	private final String SEARCHPRODUCTENDPATH = "api/v1/search/test/";

	public String getPath() {
		return SEARCHPRODUCTENDPATH;
	}

	public Response searchProduct(String product, RequestSpecification rSpec) {
		rSpec = getCommonSpec().basePath(getBasePath(product));

		return sendRequest(rSpec, Constants.RequestType.GET_REQUEST, null);
	}

	private String getBasePath(String product) {

		String tempString = "";

		tempString = getPath() + product; 

		return tempString;
	}
}
