package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;  
import endpoints.SearchProductEndPoints;   

public class SearchStepDefinitions {
	
	/**
	 * This class is the implementation of Step Definition specific to Search product
	 * 
	 */
	
	SearchProductEndPoints searchProductEndPoints = new SearchProductEndPoints();
	Validation  validation = new Validation();
 
    
    @Given("He searches for the product {string}")
    public void heCallsProducts(String product) { 
    	validation.setResponse(searchProductEndPoints.searchProduct(product, null)); 
    }
    
    @Then("He sees response has {int} status code")
	public void iSeeResponseStatusCode(int code) {
    	searchProductEndPoints.verifyResponseStatusCode(validation.getResponse(), code);
	}
    
    @Then("He sees the results displayed for {string}")
    public void resultVerify(String product) {
    	
    	searchProductEndPoints.verifyResponseData(validation.getResponse(), product);
    } 
    
    @Then("He do not see the results displayed for {string}")
    public void verifyWrongresults(String product)
    {
    	searchProductEndPoints.verifyResponseDataNotPresent(validation.getResponse(), product);
    }
    
    
}
