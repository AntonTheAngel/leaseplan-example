package steps;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Validation {
	
	/**
	 * This class is the implementation of using request and response using getters and setters.
	 * 
	 */
	
	
	private Response response;
	private RequestSpecification request;
	
	
	public Response getResponse() {
		return response;
	}
	public void setResponse(Response response) {
		this.response = response;
	}
	public RequestSpecification getRequest() {
		return request;
	}
	public void setRequest(RequestSpecification request) {
		this.request = request;
	} 

}
