package utils;

import java.util.ArrayList;
import java.util.List;

public class CommonUtils {
	
	/**
	 * This class for common utility functions used in framework.
	 * 
	 */
	
	public static final boolean containsIgnoreCase(List<String> list, String product) {

		boolean isDataPresent = false;
		
		ArrayList<String> tempList = new ArrayList<String>(list.size());
		
		tempList.addAll(list);

		if (tempList != null && !tempList.isEmpty()) {
            for (String titleString : tempList) { 
            	
                if (titleString != null && titleString.toLowerCase().contains(product)) {
                	isDataPresent =  true;
                }
            }
        }
        return isDataPresent;
	}

}