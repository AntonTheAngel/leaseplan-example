Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios 


#Positive Scenarios
   
  Scenario: Verify User searches for the product apple and he sees the results
  Given He searches for the product "apple"
  Then He sees response has 200 status code
  And He sees the results displayed for "apple"
    
   
  Scenario: Verify User searches for the product mango and he sees the results
  Given He searches for the product "mango"
  Then He sees response has 200 status code
  And He sees the results displayed for "mango"
   
   
  Scenario: Verify User searches for the product tofu and he sees the results
  Given He searches for the product "tofu"
  Then He sees response has 200 status code
  And He sees the results displayed for "tofu"
   
   
  Scenario: Verify User searches for the product water and he sees the results
  Given He searches for the product "water"
  Then He sees response has 200 status code
  And He sees the results displayed for "water"
    
    
 # Negative Scenarios   
 
 
 Scenario: Verify User searches for the product car and he does not see the results
 Given He searches for the product "car"
 Then He sees response has 404 status code
 And He do not see the results displayed for "car"  
 
 
 Scenario: Verify User searches for the product empty String and he does not see the results
 Given He searches for the product "Apple"
 Then He sees response has 404 status code
 And He do not see the results displayed for "Apple"
    